/**
 * Room class is to create a new room.
 * For example:
 * Room first = new Room("yellow", "hardwood", 1);
 * first.getWalls();
 * @author Kdq282
 *
 */


public class Room {
	String floors;
	String walls;
	int windows;
	
	/** 
	 * Initialize the Room information. 
	 * @param floors
	 * @param walls
	 * @param windows
	 */
	//Initialize to create a Room
	public Room(String floors, String walls, int windows){
		this.floors =floors;
		this.walls = walls;
		this.windows = windows;
	    }
	/**
	 * Initialize the Room information to default.
	 */
	public Room() {
		walls ="";
		floors= "";
		windows= 0;
	}
	/**
	 * Inquiry for the type of floors for the Room.
	 * @param floors
	 */
	public void setFloors(String floors)
	{
		this.floors = floors;

	}
	/**
	 * Inquiry for how many windows there is for a room.
	 * @param windows
	 */
	public void setWindows(int windows)
	{
		this.windows = windows;
		
	}
	/**
	 * Inquiry for the type of color for the Room.
	 * @param walls
	 */
	public void setWalls(String walls){
		this.walls = walls;
	}
	/**
	 * Returns to the room information
	 */
	public String getWalls()
	{
		return this.walls;
	}
	public String getFloors(){
		return this.floors;
	}
	public int getWindows(){
		return this.windows;
	}
		//From the main class it will return to the method of the String toString.
	public String toString(){
		return "Color of walls: " + this.walls + "\n" +
				"Type of floors: " + this.floors + "\n" +
				"Number of windows: " + this.windows+ "\n";
		//End
	
		
	}
}


