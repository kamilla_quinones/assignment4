
public class Main {

	

	public static void main(String[] args) {
		
		//first room 
		/**
		 * Showing the first room with getWalls.
		 */
		
		Room first = new Room("yellow", "hardwood", 1);
		first.getWalls();
		System.out.println(first);
		/**
		 * Showing the second room with getFloors
		 */
		
		//second room
		Room second = new Room("purple", "tiled", 0);
		second.getFloors();
		System.out.println(second);
		/**
		 * Showing the third room with getWindows.
		 */
		
		// third room
		Room third = new Room("white", "carpeted", 3);
		third.getWindows();
		System.out.println(third);
	    
		
		
	

	

	}

}
